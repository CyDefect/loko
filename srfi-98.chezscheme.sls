;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Partial SRFI 98 for bootstrapping with Chez Scheme

(library (srfi :98 os-environment-variables)
  (export
    get-environment-variable)
  (import
    (rnrs (6))
    (only (chezscheme) load-shared-object foreign-procedure machine-type))

(define dummy                           ;yet another copy of this funny table
  (case (machine-type)
    ((ta6le a6le i3le ti3le arm32le ppc32le) (load-shared-object "libc.so.6"))
    ((ta6fb a6fb i3fb ti3fb arm32fb ppc32fb) (load-shared-object "libc.so.7"))
    ((i3osx ti3osx a6osx ta6osx) (load-shared-object "libc.dylib"))
    ((ta6nt a6nt i3nt ti3nt) (load-shared-object "crtdll.dll"))
    (else (load-shared-object "libc.so"))))

(define get-environment-variable
  (foreign-procedure "getenv" (string) string)))
